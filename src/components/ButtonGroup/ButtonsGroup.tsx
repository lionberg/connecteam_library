import * as React from "react";
import {Children} from "react";

export interface IButtonGroup{
    children  : any
}
function ButtonGroup (props:IButtonGroup) {

        const arrayChildren = Children.toArray(props.children);

        let children = Children.map(arrayChildren, (child, index) => {
            return child;
        })
        return <div className={'ct-button-group phone-flex-direction-column'}>
            {children}
        </div>

}

export   {ButtonGroup};