import * as React from "react";
import {useEffect, useState} from "react";

export interface IButtonGroupItem {
     children?  : any,
    isSelected? : boolean,
    text        : string,
    value       : number,
    onclick?    : any

}
function ButtonGroupItem(props: IButtonGroupItem) {
    const [btnValue, setBtnValue] = useState(0);

    useEffect(() =>{
        props.onclick(btnValue);
    },[]);
         return <div key={'val'+props.value} onClick={() => {
             console.log('btn clicked: ' + props.value);
             setBtnValue(props.value);
             if (props.onclick !== undefined){
                 props.onclick(props.value);
             }
            }
         } className={'ct-button-group-item' + (props.isSelected ? ' selected': '')}>
                    {props.text}
                </div>
}

export  {ButtonGroupItem};