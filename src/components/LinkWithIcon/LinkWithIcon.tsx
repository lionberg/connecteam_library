import React, {ReactElement} from "react";
export interface LinkProps {
    label: string;
    width?: any;
    icon?: ReactElement;
    leftIcon?: ReactElement;
}

const LinkWithIcon = (props: LinkProps) => {


    return   <div style={ props.width !== undefined && props.width.toString().trim() !== "" ? {width: props.width}: {}}
                  className={'ct-link'}>
        <div className={'display-flex'}>
            <div className='ct-link-icon left display-flex align-items-center'>{props.leftIcon}</div>
            <div className='ct-link-label'>{props.label}</div>
            <div className='ct-link-icon right'>{props.icon}</div>
        </div>
    </div>;
};


export default LinkWithIcon;