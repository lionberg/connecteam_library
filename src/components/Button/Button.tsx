import React, {ReactElement} from "react";
export interface ButtonProps {
    label: string;
    width?: any;
    isSecondary?: boolean;
    icon?: ReactElement;
    cssClass?: string;
    leftIcon?: ReactElement;
}

const Button = (props: ButtonProps) => {


    return   <div style={ props.width !== undefined && props.width.toString().trim() !== "" ? {width: props.width}: {}}
                     className={ (props.isSecondary ? 'secondary' : 'primary') + ' ct-btn' + ' ' + props.cssClass}>
                    <div className={'display-flex'}>
                        <div className='ct-btn-icon left'>{props.leftIcon}</div>
                        <div className='ct-btn-label'>{props.label}</div>
                        <div className='ct-btn-icon right'>{props.icon}</div>
                    </div>
            </div>;
};


export default Button;