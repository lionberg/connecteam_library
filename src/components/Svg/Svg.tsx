import React from "react";

export interface SvgProps {
    height: any,
    width: any,
    viewBox: string,
    fill: string,
    pathFill: string,
    path: string
}

const Svg = (props: SvgProps) => {


    return  <svg width={props.width} height={props.height} fill={props.fill} viewBox={props.viewBox}>
        <path d={props.path} fill={props.pathFill}/>
    </svg>
};



export default Svg;